# Изменение размера файла данных

    * Данное руководство может быть полезно для возможных решений по
      ошибке ORA-01654

## Примечания

    * для выполнения многострочных запросов рекомендуется
      использовать команду `ed`. См. соответствущий раздел sqlplus для
      подробностей.
      * [ссылка](./sqlplus.md)
    * приведенные результаты запросов могут различаться и соответствуют
      только данному примеру

## Шаг 1. Определение текущей конфигурации

Нижеприведенные запросы являются информационными и могут служить для
оценки используемого пространства, а так-же для определения нового имени
файла данных. В раcсматриваемом примере новым файлом данных будет:
`/ora01/app/oracle/oradata/db01/users04.dbf`

```sql
select file_name from dba_data_files where tablespace_name='USERS';
```

Возможный ответ может быть таким:
```
FILE_NAME
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/ora01/app/oracle/oradata/db01/users01.dbf
/ora01/app/oracle/oradata/db01/users02.dbf
/ora01/app/oracle/oradata/db01/users03.dbf
```

Запросы для оценки свободного места:

```sql
SELECT free.tablespace_name TABLESPACE,
          ROUND (files.BYTES / 1073741824, 2) gb_total,
          ROUND ((files.BYTES - free.BYTES) / 1073741824, 2) gb_used,
          ROUND (free.BYTES / files.BYTES * 100) || '%' free
     FROM (SELECT   tablespace_name, SUM (BYTES) BYTES
               FROM dba_free_space
           GROUP BY tablespace_name) free,
          (SELECT   tablespace_name, SUM (BYTES) BYTES
               FROM dba_data_files
           GROUP BY tablespace_name) files
    WHERE free.tablespace_name = files.tablespace_name
```

Результат:
```
TABLESPACE                       GB_TOTAL    GB_USED FREE
------------------------------ ---------- ---------- -----------------------------------------
SYSAUX                                .59        .55 6%
UNDOTBS1                               48        .18 100%
USERS                                51.6      50.26 3%
SYSTEM                                .72        .71 1%
```

Более упрощенная оценка:
```sql
select sum(bytes/1024/1024/1024),sum(maxbytes/1024/1024/1024) from dba_data_files where tablespace_name='USERS';
```

Результат:
```
SUM(BYTES/1024/1024/1024) SUM(MAXBYTES/1024/1024/1024)
------------------------- ----------------------------
               51.6010742                   63.9999847
```

## Шаг 2. Изменение размера файла данных

Выполняется запрос с назначенным новым именем файла данных `users04.dbf` :
```sql
ALTER TABLESPACE USERS ADD DATAFILE
'/ora01/app/oracle/oradata/db01/users04.dbf' SIZE 8M AUTOEXTEND ON NEXT 8M MAXSIZE 16G;

```
